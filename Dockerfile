# We are going to create the image for Rasa and Rasa X. First, we use Python 3.8 as the base image.
# With this image, we are going to run some commands to install Rasa, Rasa X and its dependencies.
FROM python:3.8.10 as BASE

# Update repositories.
RUN apt-get update \
    && apt-get --assume-yes --no-install-recommends install \
    build-essential \
    curl \
    git \
    jq \
    libgomp1 \
    vim

# The working directory.
WORKDIR /app

# Update pip.
RUN pip install --no-cache-dir --upgrade pip

# Install Rasa and Rasa X
RUN pip install rasa==2.8.1
RUN pip install rasa-x==0.39.3 --extra-index-url https://pypi.rasa.com/simple

RUN pip install protobuf==3.20

# Install all the requirements. If in out actions.py we use one new module,
# we must add it to this requirements.
ADD requirements.txt requirements.txt
RUN pip install -r requirements.txt

# Add the Rasa files.
ADD config.yml config.yml
ADD domain.yml domain.yml
ADD credentials.yml credentials.yml
ADD endpoints.yml endpoints.yml