# rasa-docker

## Introduction
Hello! My name is David Mareque and I work in Learnovate. I'm doing a research about Rasa and Rasa X, two technologies to develop a chatbot. It is very hard to find the right version to work with Rasa and also be compatible with Rasa X. That's why I make this repository.

## Content
It is a Rasa project right at the beggining of its development. If you run the command docker-compose up it will deploy all the services needed:
* **Rasa:** this is the default server. It will have the enable-api so you can make request (GET, POST...) and connected to other apps. Port: 5005.
* **Rasa Actions Server:** this is the server to run the 'actions.py'. Port: 5055.
* **Duckling:** in case you need Duckling to extract entities, you will have this service running in the background so you can connect it easy. Port: 8000.
* **Rasa-X:** it is a useful tool to share your bot with other users and create stories. Port: 5002.

## Deployment
It is neccesary to have Docker Desktop running in the background. Then, in the root folder just type "docker-compose up" and it will deploy all the services. In the docker, one Rasa model will be trained aswell so it's not necessary to train it before.
